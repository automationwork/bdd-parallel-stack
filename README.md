Date : 23/05/2018
==================
NOTE : Do run

mvn clean

This will

* clean the project
* remove any instance of the old build

if we we make any changes in the build, it is always advisable to run clean. Had some issues where I removed one feature
but the test class folder under target still had the .class pointing to that feature fil, thus causing concerns

so, new command

mvn  -Dtest=SomePatternThatDontMatchAnything -DfailIfNoTests=false clean verify
================================================================================================================
Date 12/05/2018
================================================================================================================
# CucumberParallel
Demo: Run Cucumber tests faster by running them in parallel using maven plugins cucumber-jvm-parallel-plugin and maven-failsafe-plugin

Video: https://www.youtube.com/watch?v=4NJgyzvflAw

Execute in parallel using the command: 

mvn  -Dtest=SomePatternThatDontMatchAnything -DfailIfNoTests=false verify
-------------------------------------------------------------------------
Run below command and pass tag names as what to execute. Have add <tag> in POM file as variable, If not provided, will
take smoke( defined under properties) as default

----for Parallel excution of all tags----
mvn verify -Dtest=SomePatternThatDontMatchAnything -DfailIfNoTests=false -Dcucumber.options="--tags @smoke

----for Sequential excution of all tags----
mvn verify -Dcucumber.options="--tags @smoke
=====================================================================================================================

tree /f target
=====================================================================================================================
executing all tests in sequence

mvn clean test


=====================================================================================================================
---------ALL TAGGING OPTIONS-------
=====================================================================================================================

read this link  --> https://johnfergusonsmart.com/running-serenity-bdd-tests-with-tags/

1) Will run feature 1 -->
mvn clean verify  -Dtest=IgnoreTphase -DfailIfNoTests=false -Dcucumber.options="--tags @red"
2) Will run scenario anywhere with tag = blue ( under any feature) -->
mvn clean verify -Dtest=IgnoreTphase -DfailIfNoTests=false -Dcucumber.options="--tags @blue"
3) Will run scenario which will qualify 2 tags red & orange  -->
mvn clean verify -Dtest=IgnoreTphase -DfailIfNoTests=false -Dcucumber.options="--tags @red --tags @orange"
4) Will run feature or scenarios having tag as green or orange -->
mvn clean verify -Dtest=IgnoreTphase -DfailIfNoTests=false -Dcucumber.options="--tags @green,@orange"

5) Example to tag data vales under data table - refer feature1
mvn clean verify -Dtest=SomePatternThatDontMatchAnything -DfailIfNoTests=false -Dcucumber.options="--tags @green --tags @regression"

READ THIS FOR TAGGING TABLES --> https://www.coveros.com/how-to-get-the-most-out-of-cucumber-tags/
=====================================================================================================================
mvn install -Dcucumber.options="--tags @runThis --tags ~@ignore --format json-pretty:target/cucumber-report-myReport.json --format html:target/cucumber-html-report-myReport"
mvn test -Dcucumber.options="--tags @smoke
--tags ~@ignore --format json-pretty:target/cucumber-report-myReport.json --format html:target/cucumber-html-report-myReport"

mvn verify -Dcucumber.options="--tags @smoke

=====================================================================================================================

Notes

1) the POM file is real deal. Have a look at how build plugins have been configured. have a look at below
https://stackoverflow.com/questions/41034116/how-to-execute-cucumber-feature-file-parallel

2)
=====================================================================================================================
https://github.com/temyers/cucumber-jvm-parallel-plugin
=====================================================================================================================
Using TAGS under Plugin

under POM file we can define as below,
    <tags>
        <tag1>@smoke</tag1>
        <tag2>@API</tag2>
    </tags>

But the objective is to pass the same via "command" or "Jenkins" so that we can execute what ever we want
So, to do that we can do below.

    <tags>
        <tag1>${entertag}</tag1>
        <tag2>@API</tag2>
    </tags>

    and then in the properties file

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <entertag>{@smoke}</entertag>
    </properties>

 How to pass the same in jenkins-
 mvn  -Dtest=SomePatternThatDontMatchAnything -DfailIfNoTests=false -Dentertag=@smoke clean verify
 ***This works if u r defining a "tag" in the POM file

 clean test -P localsingle -DchooseEnvironment=SIT

    <profile>
             <id>localsingle</id>
             <build>
                 <plugins>
                     <plugin>
                         <groupId>org.apache.maven.plugins</groupId>
                         <artifactId>maven-surefire-plugin</artifactId>
                         <configuration>
                             <systemPropertyVariables>
                                 <chooseEnvironment>${environment}</chooseEnvironment>
                             </systemPropertyVariables>
                             <suiteXmlFiles>
                                 <suiteXmlFile>config/localsingle.testng.xml</suiteXmlFile>
                             </suiteXmlFiles>
                         </configuration>
                     </plugin>
                 </plugins>
             </build>
         </profile>

 <properties>

     <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
     <surefire.version>2.19.1</surefire.version>
     <test.file></test.file>
     <config.file>default</config.file>
     <extentreports.version>3.1.5</extentreports.version>
     <environment>SIT</environment>

 </properties>
=====================================================================================================================
 mvn  -Dtest=SomePatternThatDontMatchAnything -DfailIfNoTests=false -Dcucumber.options="--tags @smoke,@smokeTesting" clean

 mvn  -Dtest=donotrunthis -DfailIfNoTests=false -Dcucumber.options="--tags @smoke,@regression" clean verify

 ***For this you do not need to define the tag in POM file. It uses Cucumber option to point out what not to do

-Doptions="--tags @tag1 --tags @tag2"  --> This means AND condition
-Doptions="--tags @tag1,@tag2"  --> means OR condition

mvn -Dcucumber.options=--tags @smoke,@smokeTesting" clean verify
mvn test -Dcucumber.options="--tags @smoke --tags @smokeTesting"
=====================================================================================================================
NOTE : DO NOT PROVIDE TAGE NAME TO THE FEATURE BUT TO THE SCENARIOS UNDER IT. If
feature name = @smoke
Scenario name = @regression

Then the command will run all the scenarions under the feature tagged as @smoke despite of them being marked as regression.
So, it is good to have the scenarios tagged separately with no tag on the feature file. if you tag the feature file, then
try not to tag the scenarions. Not a good practice but ......
=====================================================================================================================
Port Number : 5005 change it if u need
-----------------------------------------
=====================================================================================================================
JENKINS SET-UP
=====================================================================================================================
Jenkins --> Schedule with this for every 2 minutes -> H/2 * * * *
H/1 * * * * is polling once per hour.
* */1 * * *  --> Every Minute
H */3 * * *  --> Every 3 hours  or using this 0 */3 * * *

Refer this link for more details
---------------------------------
https://superuser.com/questions/751103/need-to-schedule-a-job-every-hour-in-jenkins
-------------------------------------------------------------------------------------



=====================================================================================================================

mvn -DskipTests=false -Dcucumber.options="--tags @smoke,@smokeTesting" clean verify