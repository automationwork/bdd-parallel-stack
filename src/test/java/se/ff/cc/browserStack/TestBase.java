package se.ff.cc.browserStack;

import com.browserstack.local.Local;
import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;


import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class TestBase {
    public static WebDriver webdriver;
    private Local l;

    public WebDriver setUpBrowserStack(String browsername,String config_file ) throws Exception {

        JSONParser parser = new JSONParser();
        JSONObject config = (JSONObject) parser.parse(new FileReader("src/test/java/se/ff/cc/browserStack/conf/" + config_file));
        JSONObject envs = (JSONObject) config.get("environments");

        DesiredCapabilities capabilities = new DesiredCapabilities();

        Map<String, String> envCapabilities = (Map<String, String>) envs.get(browsername);
        Iterator it = envCapabilities.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            capabilities.setCapability(pair.getKey().toString(), pair.getValue().toString());
        }

        Map<String, String> commonCapabilities = (Map<String, String>) config.get("capabilities");
        it = commonCapabilities.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            if (capabilities.getCapability(pair.getKey().toString()) == null) {
                capabilities.setCapability(pair.getKey().toString(), pair.getValue().toString());
            }
        }

        String username = System.getenv("BROWSERSTACK_USERNAME");
        if (username == null) {
            username = (String) config.get("user");
        }

        String accessKey = System.getenv("BROWSERSTACK_ACCESS_KEY");
        if (accessKey == null) {
            accessKey = (String) config.get("key");
        }

        if (capabilities.getCapability("browserstack.local") != null && capabilities.getCapability("browserstack.local") == "true") {
            l = new Local();
            Map<String, String> options = new HashMap<String, String>();
            options.put("key", accessKey);
            l.start(options);
        }
       return webdriver = new RemoteWebDriver(new URL("http://" + username + ":" + accessKey + "@" + config.get("server") + "/wd/hub"), capabilities);

//        Choose as whcih enviornment this need to be executed.
//        This is populated from POM file which has defualt value as defined in Properties
//        But can take value from jenkins or command line

//        String value = System.getProperty("chooseEnvironment");
//        System.out.println("Automation will run in " + value + " environment.");
//        if (value.equalsIgnoreCase("SIT")) {
//            driver.get("http://automationpractice.com/index.php");
////            driver.manage().window().maximize();
//        } else if (value.equalsIgnoreCase("UAT")) {
//            driver.get("https://www.bbc.co.uk/");
////            driver.manage().window().maximize();
//        } else {
//            System.out.println("You have not selected an enviornment. Automatiion will now terminate");
//            teardown();
//        }
    }

//    @AfterMethod(alwaysRun = true)
//    public void teardown() throws Exception {
////    public void tearDown(ITestResult result) throws Exception {
////        if (result.getStatus() == ITestResult.FAILURE) {
////            test.log(Status.FAIL, MarkupHelper.createLabel(result.getName() + "TEST CASE FAIL", ExtentColor.RED));
////            test.fail(result.getThrowable());
////
////            String screenshotPath = getScreenshot(driver, result.getName());
////            test.log(Status.FAIL, String.valueOf(test.addScreenCaptureFromPath(screenshotPath))); //to add screenshot in extent report
//////            test.log(Status.FAIL, (Markup) test.addScreencastFromPath(screenshotPath)); //to add screencast/video in extent report
////
////        } else if (result.getStatus() == ITestResult.SUCCESS) {
////            test.log(Status.PASS, MarkupHelper.createLabel(result.getName() + "TEST CASE PASS", ExtentColor.GREEN));
////        } else if (result.getStatus() == ITestResult.SKIP) {
////            test.log(Status.SKIP, MarkupHelper.createLabel(result.getName() + "TEST CASE FAIL", ExtentColor.YELLOW));
////            test.fail(result.getThrowable());
////        }
//        webdriver.quit();
//        if (l != null) l.stop();
//    }

//    @AfterSuite
//    public void endReport() {
//        extent.flush();
//    }


}
