//package se.ff.cc.helpers;
//
//import com.aventstack.extentreports.ExtentReporter;
//import com.aventstack.extentreports.ExtentReports;
//import com.aventstack.extentreports.ExtentTest;
//import com.aventstack.extentreports.markuputils.ExtentColor;
//import com.aventstack.extentreports.markuputils.MarkupHelper;
//import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
//import org.testng.ITestResult;
//import org.testng.annotations.*;
//
//public class BaseTest {
//
//    public static ExtentReporter htmlReporter;
//    public static ExtentReports extent;
//    public static ExtentTest test;
//
//    @BeforeClass
//    public void setup(){
//        htmlReporter = new ExtentHtmlReporter("reports\\res.html");
//        extent = new ExtentReports();
//        extent.attachReporter(htmlReporter);
//    }
//
//    @AfterClass
//    public void getResults(ITestResult result){
//        if(result.getStatus()==ITestResult.FAILURE){
//            test.fail(MarkupHelper.createLabel(result.getName() + "Test failed", ExtentColor.RED));
//        }
//    }
//
//    @AfterSuite
//    public void teardown(){
//        extent.flush();
//    }
//}
