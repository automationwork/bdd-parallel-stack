package se.ff.cc.helpers;

import com.github.javafaker.Faker;

public class generateTestData {

    private Faker faker = new Faker();
    private String name = faker.name().fullName();
    private String firstName = faker.name().firstName();
    private String lastName = faker.name().lastName();
    private String email = firstName + "." + lastName + "@gmail.com";
    private String streetAddress = faker.address().streetAddress();

    public String getName() {return name;}
    public String getFirstName() {return firstName;}
    public String getLastName() {return lastName;}
    public String getEmail() {return email;}
    public String getStreetAddress() {return streetAddress;}

}



