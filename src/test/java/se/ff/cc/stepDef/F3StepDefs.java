package se.ff.cc.stepDef;

import cucumber.api.PendingException;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import se.ff.cc.browserStack.TestBase;
import se.ff.cc.pages.RetailUser;
import se.ff.cc.webDriver.Driver;

import static se.ff.cc.webDriver.Driver.*;

public class F3StepDefs {

    private String Password = "Password1234";
    private TestBase tb = new TestBase();

    @When("^I have logged in browser stack \"([^\"]*)\" \"([^\"]*)\"$")
    public void iHaveLoggedInBrowserStack(String browsername, String configfile) throws Exception {

        RetailUser login = new RetailUser(tb.setUpBrowserStack(browsername,configfile));
        login.login();
    }
    @When("^I am on the access the home page of the application$")
    public void iAmOnTheAccessTheHomePageOfTheApplication() throws Throwable {
        RetailUser createuser = new RetailUser(tb.webdriver);
        createuser.newusercreationsteps(Password);
        throw new PendingException();
    }
}
