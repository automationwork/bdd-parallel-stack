package se.ff.cc.stepDef;


import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import se.ff.cc.pages.RetailUser;
import se.ff.cc.webDriver.Driver;

public class RetailShopping {
    String Password = "Password1234";

    @When("^I am on the home page and login using \"([^\"]*)\"$")
    public void iAmOnTheHomePageAndLoginUsing(String browser) throws Throwable {
        RetailUser login = new RetailUser(Driver.getCurrentDriver(browser));
        login.login();
    }
    @Then("^I provide email as \"([^\"]*)\"$")
    public void iProvideEmailAs(String email) throws Throwable {
        RetailUser createuser = new RetailUser(Driver.webdriver);
        createuser.newusercreationsteps(Password);
    }

}
