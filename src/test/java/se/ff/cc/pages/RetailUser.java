package se.ff.cc.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import se.ff.cc.helpers.generateTestData;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class RetailUser {
    WebDriver driver;

    String filepath = "testdata\\userlogindata.txt";

    private By login = By.cssSelector(".login");
    private By email = By.id("email_create");
    private By createaccount = By.id("SubmitCreate");
    private By gender = By.id("uniform-id_gender1");
    private By customerFirstname = By.id("customer_firstname");
    private By customerLastname = By.id("customer_lastname");
    private By password = By.id("passwd");
    private By day = By.id("days");
    private By month = By.id("months");
    private By year = By.id("years");
    private By firstname = By.id("firstname");
    private By lastname = By.id("lastname");
    private By company = By.id("company");
    private By address1 = By.id("address1");
    private By city = By.id("city");
    private By state = By.id("id_state");
    private By postcode = By.id("postcode");
    private By phone = By.id("phone");
    private By mobile = By.id("phone_mobile");
    private By submitAccount = By.id("submitAccount");
    private By error = By.cssSelector(".alert.alert-danger>ol>li");

    public RetailUser(WebDriver driver) {
        this.driver = driver;
    }

    public void login() {
        driver.get("http://automationpractice.com/index.php");
        driver.findElement(login).click();
    }

    public void newusercreationsteps(String Password) {

        generateTestData data = new generateTestData();
        String dummy_email = data.getEmail();
        String dummy_firstname = data.getFirstName();
        String dummy_lastname = data.getLastName();

//        readWrite write = new readWrite();
//        write.writing(dummy_email, filepath);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//        driver.manage().window().maximize();
//        login();
        driver.findElement(email).sendKeys(dummy_email);
        driver.findElement(createaccount).submit();
        driver.findElement(gender).click();
        driver.findElement(customerFirstname).sendKeys(dummy_firstname);
        driver.findElement(customerLastname).sendKeys(dummy_lastname);
        driver.findElement(password).sendKeys(Password);

        driver.findElement(day).click();
        Select sdate = new Select(driver.findElement(day));
        sdate.selectByIndex(5);

        driver.findElement(month).click();
        Select smonth = new Select(driver.findElement(month));
        smonth.selectByIndex(5);

        driver.findElement(year).click();
        Select syear = new Select(driver.findElement(year));
        syear.selectByValue("1960");
        driver.findElement(year).click();

        driver.findElement(firstname).clear();
        driver.findElement(firstname).sendKeys(dummy_firstname);
        driver.findElement(lastname).clear();
        driver.findElement(lastname).sendKeys(dummy_lastname);

        driver.findElement(company).sendKeys("test company");
        driver.findElement(address1).sendKeys("Manchester");
        driver.findElement(city).sendKeys("Manchester");

        driver.findElement(state).click();
        Select sstate = new Select(driver.findElement(state));
        sstate.selectByValue("47");
        driver.findElement(state).click();

        driver.findElement(postcode).sendKeys("193457");
        driver.findElement(phone).sendKeys("1290876645");
        driver.findElement(mobile).sendKeys("87686969669");

//        driver.findElement(submitAccount).click();

        try {
            List<WebElement> elems = driver.findElements(error);
            Iterator<WebElement> iter = elems.iterator();
            System.out.println("Check Below Error(s)");
            while (iter.hasNext()) {
                WebElement item = iter.next();
                String err = item.getText();
                System.out.println(err);
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }
}





//    public static WebElement registerNow() {
//        return driver.findElement(By.className("btn-border-grey"));
//    }
//
//    public static WebElement firstName() {
//        return driver.findElement(By.id("firstnameUKI"));
//    }
//
//    public static WebElement surname() {
//        return driver.findElement(By.id("surname"));
//    }
//
//    public static WebElement email() {
//        return driver.findElement(By.id("email"));
//    }
//
//    public static WebElement password() {
//        return driver.findElement(By.id("password"));
//    }
//
//    public static WebElement confirmpassword() {
//        return driver.findElement(By.id("confirm-password"));
//    }
//
//    public static WebElement selectSecurityQuestions() {
//        return driver.findElement(By.id("SQ-select-1"));
//    }
//
//}
//
//
////
////    @FindBy(how = How.ID, using = "questionList-0-answer")
////    public WebElement q1Answer;
////
////    @FindBy(how = How.CLASS_NAME, using = "tick")
////    List<WebElement> allTicks;
////
////    @FindBy(how = How.CSS, using = ".hidden-sm.hidden-xs.pull-left.desktop-vs-logo>a>img")
////    public WebElement homeicon;
////
////    @FindBy(how = How.CSS, using = "#top")
////    public WebElement goTop;
